Getting Started
===
This tutorial will get you started with `C++`.

By the end of this tutorial you'll be using:
- `GCC`: For compiling your code.
- `CMake`: For managing the build of your project.
- `ninja-build`: For quickly coordinating the build of your project (CMake uses this).
- `GDB`: For debugging your program.
- `vscode (Visual Studio Code)`: For your workspace, it will be where you edit and debug your code.

# Windows
To start, we are going to use [`winget`](https://www.microsoft.com/en-au/p/app-installer/9nblggh4nns1) to get you started as fast as possible. Everything you need will either be installed from [`winget`](https://www.microsoft.com/en-au/p/app-installer/9nblggh4nns1) or from links provided here.

1. Click here to get [`winget`](https://www.microsoft.com/en-au/p/app-installer/9nblggh4nns1)!
Note: 

> Windows now ships with `winget`, if you have an up to date windows installation, this step can be skipped.

2. Install `vscode`:
   1. Open powershell
   2. Run `winget install -e --id Microsoft.VisualStudioCode`, programming in notepad can only get you so far.

3. Install `CMake`:
   1. Open powershell
   2. Run `winget install -e --id Kitware.CMake`, this will make building programs easy.

4. Install `MSYS2`, we will use this to get our compiler and some other essential goodies:
   1. Open powershell
   2. Run `winget install -e --id msys2.msys2`, this is a tool used to install our other dependancies.

5. Install `MSYS2` packages (GCC, ninja-build, GDB):
   1. Open `MSYS2 MSYS` (if you cannot find it, search in start, it should appear).
   2. Run `pacman -Syu`, this will update the `MSYS2` database, this will close the MSYS2 terminal.
   3. Re-open `MSYS2 MSYS`, it might take a moment for it to reappear in start.
   4. Run `pacman -S base-devel`, this will give you the essentials for building packages.
   5. Run `pacman -S mingw-w64-x86_64-toolchain`, this will install `GCC` and `GDB`. You will be prompted and asked which package to install, the simpliest option is to press enter (leaving the input blank) and install all.
   5. Run `pacman -S mingw-w64-x86_64-ninja`, this will install `ninja-build`.

# All Platforms
6. Setup plugins in `vscode`:
   1. Open `vscode` (if you cannot find it, search `vscode` in start, it should appear).
   2. Press `Ctrl + Shift + X` (open the extensions panel). 
   3. Search for `C/C++` and install the result from the author `Microsoft`, this will give you intellisense(fancy name for code autocomplete) and debugging.
   4. Search for `CMake Tools` and install the result from author `Microsoft`, this will give you the ability to compile and manage your project. It'll also make debugging a breeze.
   5. Search for `CMake` and install the result from author `twxs`, this will ensure your `cmake` scripts (don't worry, our example will be 3 lines) have syntax highlighting and some intellisense.

7. Lets create a project in `vscode`!
   1. Create a new folder in explorer for your project to reside.
   2. Right click on the folder and click `Open with Code`.
   3. Create a new file with `Ctrl + N`, save it immediately with `Ctrl + S`, name it `CMakeLists.txt`. This specific naming is required for CMake.
   4. Paste the following into your `CMakeLists.txt` file:
      You can start with this CMake script:
         ```CMAKE
         cmake_minimum_required(VERSION 3.16.0)
         project(YourProjectNameHere VERSION 0.1.0 LANGUAGES CXX)
         #       ^ put your project name here
         add_executable(${PROJECT_NAME} main.cpp)
         # 							   ^ put all your .c/.cpp files here (don't include headers).
         ```

   5. Create a new file with `Ctrl + N`, save it immediately with `Ctrl + S`, name it `main.cpp` (a new source file).
      You can start with this `C++` source script:
         ```C++
         #include <cstdio>
         int main()
         {
            std::puts("Hello, world");
         }
         ```

   6. Use `GCC` and `CMake` for your project:
      1. Press `Ctrl + Shift + P` this will open the command bar in vscode.
      2. Enter command `CMake: Delete Cache and Reconfigure` and press enter.
      3. If a window pops asking to `Select a kit`, select the item called `GCC X.Y.Z...`.
      4. If a window did not pop up enter command `CMake: Scan for Kits` and press enter.

   7. Time to compile:
      1. Press `Ctrl + Shift + P` this will open the command bar in vscode.
      2. Enter command `CMake: Clean Rebuild` and press enter.
      3. The compiler should output something that looks like this:

         ```PS1
         [main] Building folder: setup_vscode_tutorial clean
         [build] Starting build
         [proc] Executing command: "C:\Program Files\CMake\bin\cmake.EXE" --build f:/Files/Git/setup_vscode_tutorial/build --config Debug --target clean -j 34 --
         [build] [1/1 100% :: 0.039] Cleaning all built files...
         [build] Cleaning... 2 files.
         [build] Build finished with exit code 0
         [main] Building folder: setup_vscode_tutorial 
         [build] Starting build
         [proc] Executing command: "C:\Program Files\CMake\bin\cmake.EXE" --build f:/Files/Git/setup_vscode_tutorial/build --config Debug --target all -j 34 --
         [build] [1/2  50% :: 0.073] Building CXX object CMakeFiles/YourProjectNameHere.dir/main.cpp.obj
         [build] [2/2 100% :: 0.181] Linking CXX executable YourProjectNameHere.exe
         [build] Build finished with exit code 0
         ```
   8. Time to run your code, to do this we will configure `GDB`, this will also allow us to debug code:
      1. Press `Ctrl + Shift + E` to navigate to the `Explorer` panel in vscode.
      2. Find the `.vscode` folder, inside it create a file `launch.json`, inside this file paste the following:
         ```JSON
         {
            "version": "0.2.0",
            "configurations": [
               {
                     "name": "GDB",
                     "type": "cppdbg",
                     "request": "launch",
                     "args": [],
                     "program": "${command:cmake.launchTargetPath}",
                     "stopAtEntry": true,
                     "cwd": "${workspaceFolder}",
                     "MIMode": "gdb",
                     "miDebuggerPath": "gdb.exe",
                     "showDisplayString": true
               }
            ]
         }
         ```
      3. Press `Ctrl + Shift + D` to navigate to the `Run and Debug` panel in vscode.
      4. Click the play icon at the top!

Great work, this is a great start with your `C++` journey.
